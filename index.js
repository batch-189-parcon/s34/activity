const express = require('express')

const app = express()

const port = 3000

app.use(express.json())
app.use(express.urlencoded({extended: true}))

app.get('/home', (request, response) => {
	response.send('Welcome to the home page')
})

let users = []

app.get('/users', (request, response) => {
	console.log(request.body)

	if(request.body.username !== '' && request.body.password !== '') {
			users.push(request.body)
			
		}
	response.send(users)
})


app.delete('/delete-user', (request, response) => {
	let message
	
	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){
			message = `User ${request.body.username} has been deleted!`

			break
		} else {
			message = 'User does not exist.'
		}
}

response.send(message)

})




app.listen(port, () => console.log(`Server is running at port ${port}`))